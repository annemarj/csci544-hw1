from string import ascii_lowercase as letters1
from string import ascii_uppercase as letters2
import json
import sys

def keyCheck(key, arr):
 if key in arr.keys():
  return "initialised"
 else:
  return "notInitialised"

with open(sys.argv[1]) as json_file:
 json_data = json.load(json_file)

#output=open("spam.out",'w')
sumOfLog={}
myInput=open(sys.argv[2], 'r')
for line in myInput:
 #print("bingo")
 #print(line)
 temp1=-10000000
 
 mylist_character=""
 for character in line:	
  if character in letters1 or character in letters2:
   mylist_character+=character.lower()
    
  else:
    
   if mylist_character!="":
    counter=0
   
    while(counter<json_data['numberOfClasses']):
     var=json_data['list_class']
     if mylist_character in json_data['mainDict'][var[counter]].keys():
      if(keyCheck(var[counter],sumOfLog)=="notInitialised"):
       #print(mylist_character)
       #print("not initialised,word in class")
       #print(var[counter])
       sumOfLog[var[counter]]=json_data['priorProb'][var[counter]]+json_data['mainDict'][var[counter]][mylist_character]
       #print(sumOfLog[var[counter]])
      else:
       #print(mylist_character)
       sumOfLog[var[counter]]+=json_data['mainDict'][var[counter]][mylist_character]
       #print("already initialised,word in class")
       #print(var[counter])
       #print(sumOfLog[var[counter]])
     else:
      if(keyCheck(var[counter],sumOfLog)=="notInitialised"):
       #print(mylist_character)
       #print("not initialised,word not in")
       #print(var[counter])
       sumOfLog[var[counter]]=json_data['priorProb'][var[counter]]+json_data['probAbsentWord'][var[counter]]
       #print(sumOfLog[var[counter]])
      else:
       #print(mylist_character)
       
       sumOfLog[var[counter]]+=json_data['probAbsentWord'][var[counter]]
       #print("already initialised,word not in") 
       #print(var[counter])
       #print(sumOfLog[var[counter]])

     counter+=1
  
    mylist_character=""
 c=0

 while(c<json_data['numberOfClasses']):
  var1=json_data['list_class']
  if sumOfLog[var1[c]]>temp1: 
   temp1=sumOfLog[var1[c]]
   temp2=var1[c]
  c+=1
 #print("final temp2:")
 print(temp2)
 #output.write('%s\n'%temp2)

 sumOfLog={}
 


