from string import ascii_lowercase as letters1
from string import ascii_uppercase as letters2

import sys
import os
import re

#correctClassification=open("reqdResults2.txt",'w')

myOutput=open(sys.argv[2],'w')
for files in sorted(os.listdir(sys.argv[1])):
 #result = re.search('([A-Z]|[a-z])*', files)
 #correctClassification.write(result.group())
 #correctClassification.write("\n")

 path=os.getcwd()+"/"+sys.argv[1]+"/"+files
 myInput=open(path,"r",encoding = "utf-8",errors="ignore")
 for line in myInput: 
   for character in line:
     if character!='\n':
       myOutput.write('%s'%character)
     else:
       myOutput.write(" ")
 myOutput.write("\n")
