from string import ascii_lowercase as letters1
from string import ascii_uppercase as letters2
from collections import Counter

import json
import sys
import math

list_class=[]
mainDict={}
dictClassNumber={}
priorProb={}
wordGivenClass={}
totalWordsEachClass={}
probAbsentWord={}
vocab=[]
probEachWord={}

default=0
numLines=0
base=10

def keyCheck(key, arr):
    if key in arr.keys():
        return "initialised"
    else:
        return "notInitialised"

def getClass(a):
  i=0 
  while a[i]!=' ':
    i=i+1
  clas=a[0:i] 
  return clas

myInput=open(sys.argv[1], 'r')
for line in myInput:
  numLines+=1
  myDict={} 
  myStr="";
  j=-1
  c=0	
  for character in line:	
   myStr+=character
   c=c+1
   if c==15:
     break
  
  #print("class:"+getClass(myStr))
  if(keyCheck(getClass(myStr), dictClassNumber)=="notInitialised"):
   dictClassNumber[getClass(myStr)]=1
   j=j+1
   list_class.append(getClass(myStr))
   priorProb[getClass(myStr)]=0
  else:
   dictClassNumber[getClass(myStr)]+=1
  
  mylist_character=""
  for character in line:	
    if character in letters1 or character in letters2:
     mylist_character+=character.lower()
    else:
     if mylist_character!="":
      if(keyCheck(mylist_character,myDict)=="notInitialised"):
       myDict[mylist_character]=1
      else:
       myDict[mylist_character]+=1
      mylist_character=""
 
  if dictClassNumber[getClass(myStr)]==1:
    mainDict[getClass(myStr)]=myDict
    """print("first:")
    print(getClass(myStr))
    print(mainDict[getClass(myStr)])"""
  else:
    
    first=Counter(mainDict[getClass(myStr)])
    second=Counter(myDict)
    mainDict[getClass(myStr)]=dict(first+second)


k=0
sum=0
while k<len(list_class):
 print(list_class[k])
 sum+=dictClassNumber[list_class[k]]
 k=k+1


k=0
while k<len(list_class):
 priorProb[list_class[k]]=math.log(((dictClassNumber[list_class[k]])/sum),base)
 
 print(priorProb[list_class[k]])
 k=k+1

for item in mainDict:
 for i in mainDict[item]:
    if i not in vocab:
      vocab.append(i)
    
    if(keyCheck(item,totalWordsEachClass)=="notInitialised"):
     totalWordsEachClass[item]=mainDict[item][i]
    else:
     totalWordsEachClass[item]+=mainDict[item][i]
 totalWordsEachClass[item]-=dictClassNumber[item]

count_distinctWords=len(vocab)-len(list_class)
print(count_distinctWords)

k=0
while k<len(list_class):
 var=count_distinctWords+totalWordsEachClass[list_class[k]]
 print (var)
 probAbsentWord[list_class[k]]=math.log(1,base)-math.log(var,base)
 k=k+1

for item in mainDict:
 for i in mainDict[item]:
   mainDict[item][i]=math.log((1+mainDict[item][i]),base)-math.log((count_distinctWords+totalWordsEachClass[item]),base)


jsonData={}

jsonData['priorProb']=priorProb
jsonData['mainDict']=mainDict
#jsonData['count_distinctWords']=count_distinctWords
#jsonData['totalWordsEachClass']=totalWordsEachClass
jsonData['probAbsentWord']=probAbsentWord
#jsonData['probEachWord']=probEachWord
jsonData['numberOfClasses']=len(list_class)
jsonData['list_class']=list_class


with open(sys.argv[2],"w+") as out_file:
  json.dump(jsonData,out_file,indent=4)


 


