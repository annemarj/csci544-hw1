from string import ascii_lowercase as letters1
from string import ascii_uppercase as letters2

import sys
import os
import re

myOutput=open(sys.argv[2],'w')
#with open(sys.argv[1], 'r') as myInput:
for files in os.listdir(sys.argv[1]):
 print (files)
 result = re.search('([A-Z]|[a-z])*', files)
 #print (result.group()) 
 myOutput.write(result.group())
 myOutput.write(" ")
 path=os.getcwd()+"/"+sys.argv[1]+"/"+files
 myInput=open(path,"r",encoding = "utf-8",errors="ignore")
 for line in myInput: 
   for character in line:
     if character!='\n':
       myOutput.write('%s'%character)
     else:
       myOutput.write(" ")
 myOutput.write("\n")
